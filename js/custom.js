jQuery(document).ready(function () {
    
    //Wow
    wow = new WOW({
        mobile: false // default
    })
    wow.init();
    
});

//Header

jQuery(window).on("load resize scroll", function(e) {
  var Win = jQuery(window).height();
  var Header = jQuery("header").height();
  var Footer = jQuery("footer").height();

  var NHF = Header + Footer;

  jQuery('.main').css('min-height', (Win - NHF));

});

//Page Zoom

document.documentElement.addEventListener('touchstart', function (event) {
 if (event.touches.length > 1) {
   event.preventDefault();
 }
}, false);

//Model

jQuery(function() {
  //----- OPEN
  jQuery('[data-popup-open]').on('click', function(e) {
    var targeted_popup_class = jQuery(this).attr('data-popup-open');
    jQuery('[data-popup="' + targeted_popup_class + '"]').fadeIn(100);

    e.preventDefault();
  });

  //----- CLOSE
  jQuery('[data-popup-close]').on('click', function(e) {
    var targeted_popup_class = jQuery(this).attr('data-popup-close');
    jQuery('[data-popup="' + targeted_popup_class + '"]').fadeOut(200);

    e.preventDefault();
  });
});

//Textarea 

jQuery(function (jQuery) {
    jQuery('.firstCap, textarea').on('keypress', function (event) {
        var jQuerythis = jQuery(this),
            thisVal = jQuerythis.val(),
            FLC = thisVal.slice(0, 1).toUpperCase();
        con = thisVal.slice(1, thisVal.length);
        jQuery(this).val(FLC + con);
    });
});

// Aditional Info

jQuery(".anything-wrap").each(function () {

    var inClick = jQuery(this).find(".form-check-label input[type='checkbox']");

    var enForm = jQuery(this).find(".form-sec");

    inClick.click(function () {
        if (inClick.is(":checked")) {
            enForm.show(300);
        } else {
            enForm.hide(300);
        }
    });
});

// Date Picker
  
jQuery(function() {
    jQuery( ".datepicker" ).datepicker();
});

// Selectpicker
jQuery('.selectpicker').selectpicker();